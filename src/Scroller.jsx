import React from 'react';
import PropTypes from 'prop-types';

import useInfiniteScroll from './useInfiniteScroll';

export default function Scroller({ loading, loadNext }) {
  const ref = useInfiniteScroll(loadNext, loading);

  return (
    <div ref={ref} className="scroll-loader">
      {loading ? 'Loading items' : ''}
    </div>
  );
}

Scroller.propTypes = {
  loading: PropTypes.bool.isRequired,
  loadNext: PropTypes.func.isRequired,
};
