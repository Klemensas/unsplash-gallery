import React from 'react';

import PhotoItem from './PhotoItem';
import { useFavourite, useLoadNextPhotoPage } from './store/actions';
import Scroller from './Scroller';
import ErrorMessage from './Error';
import { useStoreState } from './store/state';

export default function PhotoList() {
  const {
    photoIds,
    photoEntities,
    favourites,
    loading,
    hasMore,
    error,
  } = useStoreState();
  const toggleFavourite = useFavourite();
  const loadPhotoPage = useLoadNextPhotoPage();
  const photos = photoIds.map(id => photoEntities[id]);
  const scroller =
    hasMore && !error ? (
      <Scroller loading={loading} loadNext={loadPhotoPage} />
    ) : null;
  const errorMessage = error ? <ErrorMessage error={error} /> : null;
  const photoItems = photos.map(photo => (
    <PhotoItem
      key={photo.id}
      photo={photo}
      isFavourite={favourites.includes(photo.id)}
      toggleFavourite={id => toggleFavourite(id)}
    />
  ));

  return (
    <React.Fragment>
      <div className="photo-layout">
        {photoItems}
        {scroller}
      </div>
      {errorMessage}
    </React.Fragment>
  );
}
