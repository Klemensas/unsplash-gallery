import { useRef, useEffect } from 'react';

export default function useInfiniteScroll(
  loadCallback,
  isLoading,
  rootMargin = '200px',
) {
  const ref = useRef();

  useEffect(() => {
    const observer = new IntersectionObserver(
      ([entry]) => {
        if (!entry.isIntersecting || isLoading) {
          return;
        }

        loadCallback();
      },
      {
        rootMargin,
      },
    );

    if (ref && ref.current) {
      observer.observe(ref.current);
    }

    return () => observer.disconnect();
  }, [isLoading]);

  return ref;
}
