import React from 'react';
import PropTypes from 'prop-types';

export const genericError = 'Something failed';

export default function ErrorMessage({ error }) {
  const message = error && error.toString ? error.toString() : genericError;
  return <div className="error-message">{message}</div>;
}

ErrorMessage.propTypes = {
  error: PropTypes.node.isRequired,
};
