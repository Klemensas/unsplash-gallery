import React from 'react';
import PropTypes from 'prop-types';

import LazyImage from './LazyImage';

export const sizeSettings = [
  '(max-width: 599px) 100vw',
  '(max-width: 1199px) 33.33vw',
].join(', ');
export const srcSetSettings = [
  ['thumb', 200],
  ['small', 400],
  ['regular', 1080],
];

export default function PhotoItem(props) {
  const { photo, isFavourite, toggleFavourite } = props;
  const srcSet = srcSetSettings
    .map(([type, size]) => `${photo.urls[type]} ${size}w`)
    .join(', ');

  return (
    <section className="photo-box">
      <LazyImage
        srcset={srcSet}
        sizes={sizeSettings}
        src={photo.urls.raw}
        alt={photo.alt_description || photo.description || photo.id}
      />
      <div className="photo-content">
        <div className="photo-content-description">
          {photo.description || photo.alt_description || photo.user.username}
        </div>
        <div className="photo-content-separator" />
        <div className="photo-content-author">{photo.user.name}</div>
        <button
          className={`photo-content-btn ${isFavourite ? 'is-selected' : ''}`}
          type="button"
          onClick={() => toggleFavourite(photo.id)}
        >
          {isFavourite ? 'Unfavourite' : 'Favourite'}
        </button>
      </div>
    </section>
  );
}

PhotoItem.propTypes = {
  photo: PropTypes.shape({
    id: PropTypes.string,
    description: PropTypes.string,
    alt_description: PropTypes.string,
    user: PropTypes.shape({
      name: PropTypes.string,
      username: PropTypes.string,
    }),
    urls: PropTypes.shape({
      thumb: PropTypes.string,
      small: PropTypes.string,
      regular: PropTypes.string,
      full: PropTypes.string,
    }),
  }).isRequired,
  isFavourite: PropTypes.bool.isRequired,
  toggleFavourite: PropTypes.func.isRequired,
};
