import React from 'react';
import ReactDOM from 'react-dom';

import './index.scss';
import { Provider } from './store/state';
import PhotoList from './PhotoList';

ReactDOM.render(
  <Provider>
    <PhotoList />
  </Provider>,
  document.getElementById('root'),
);

if (module.hot) {
  module.hot.accept('./PhotoList', () => {
    ReactDOM.render(
      <Provider>
        <PhotoList />
      </Provider>,
      document.getElementById('root'),
    );
  });
}
