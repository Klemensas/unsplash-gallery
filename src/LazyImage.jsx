import React from 'react';
import PropTypes from 'prop-types';

import useLazyLoad from './useLazyLoad';

export default function LazyImage({ src, sizes, srcset, alt }) {
  const ref = useLazyLoad({ src, sizes, srcset });
  return <img alt={alt} ref={ref} />;
}

LazyImage.propTypes = {
  srcset: PropTypes.string.isRequired,
  sizes: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
};
