import { useRef, useEffect } from 'react';

export default function useLazyLoad(
  { src, sizes, srcset },
  rootMargin = '200px',
) {
  const ref = useRef();

  useEffect(() => {
    const observer = new IntersectionObserver(
      ([entry]) => {
        if (!entry.isIntersecting) {
          return;
        }

        ref.current.src = src;
        ref.current.sizes = sizes;
        ref.current.srcset = srcset;
        observer.disconnect();
      },
      {
        rootMargin,
      },
    );

    if (ref && ref.current) {
      observer.observe(ref.current);
    }

    return () => observer.disconnect();
  }, []);

  return ref;
}
