import fetchPhotoPage from '../api';
import { useDispatch, useStoreState } from './state';

export const actionTypes = {
  loadPhotoPage: 'Load Photo Page',
  loadPhotoPageSuccess: 'Load Photo Page Success',
  loadPhotoPageFail: 'Load Photo Page Fail',
  toggleFavourite: 'Toggle Favourite',
};

export const useLoadNextPhotoPage = () => {
  const dispatch = useDispatch();
  const { nextPage, perPage } = useStoreState();

  return async () => {
    dispatch({
      type: actionTypes.loadPhotoPage,
      payload: { pending: true },
    });

    try {
      const payload = await fetchPhotoPage(nextPage, perPage);
      dispatch({
        payload,
        type: actionTypes.loadPhotoPageSuccess,
      });
    } catch (error) {
      dispatch({
        type: actionTypes.loadPhotoPageFail,
        payload: error,
      });
    }
  };
};

export const useFavourite = () => {
  const dispatch = useDispatch();
  return payload => dispatch({ type: actionTypes.toggleFavourite, payload });
};
