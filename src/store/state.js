import createStore from './store';
import { reducer, initialState } from './reducer';
import { restoreState, storeState } from './storage';

const stateStorage = storeState(['favourites']);

export const { Provider, useStoreState, useDispatch } = createStore(
  reducer,
  initialState,
  restoreState,
  [stateStorage],
);
