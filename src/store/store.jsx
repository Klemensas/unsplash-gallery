import React from 'react';
import PropTypes from 'prop-types';

function compose(fns) {
  if (fns.length === 1) {
    return fns[0];
  }

  return (...args) => fns.reduce((arg, fn) => fn(arg), args);
}

function getReducerFn(reducer, reducerHelpers) {
  if (!reducerHelpers.length) {
    return reducer;
  }

  const fns = compose(reducerHelpers);
  return (...args) => {
    const state = reducer(...args);
    fns({ ...state });

    return state;
  };
}

export default function createStore(
  reducer,
  initialState,
  stateInitializer = state => state,
  stateChangeEffects = [],
) {
  const StoreContext = React.createContext();
  const DispatchContext = React.createContext();
  const reducerFn = getReducerFn(reducer, stateChangeEffects);

  const useDispatch = () => React.useContext(DispatchContext);
  const useStoreState = () => React.useContext(StoreContext);

  function Provider({ children }) {
    const [state, dispatch] = React.useReducer(
      reducerFn,
      initialState,
      stateInitializer,
    );

    return (
      <StoreContext.Provider value={state}>
        <DispatchContext.Provider value={dispatch}>
          {children}
        </DispatchContext.Provider>
      </StoreContext.Provider>
    );
  }
  Provider.propTypes = {
    children: PropTypes.element.isRequired,
  };

  return {
    StoreContext,
    DispatchContext,
    Provider,
    useDispatch,
    useStoreState,
  };
}
