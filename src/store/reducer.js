import { actionTypes } from './actions';

export const initialState = {
  photoIds: [],
  photoEntities: {},
  favourites: [],
  perPage: 30,
  nextPage: 1,
  loading: false,
  hasMore: true,
  error: null,
};

export function reducer(state, action) {
  switch (action.type) {
    case actionTypes.loadPhotoPage: {
      return { ...state, loading: true, error: null };
    }

    case actionTypes.loadPhotoPageFail: {
      return { ...state, loading: false, error: action.payload };
    }

    case actionTypes.loadPhotoPageSuccess: {
      const { data, hasMore } = action.payload;
      const { ids, entities } = data.reduce(
        (acc, item) => {
          if (!acc.entities[item.id]) {
            acc.ids.push(item.id);
          }
          acc.entities[item.id] = item;
          return acc;
        },
        { ids: [...state.photoIds], entities: { ...state.photoEntities } },
      );

      return {
        ...state,
        hasMore,
        loading: false,
        nextPage: state.nextPage + 1,
        photoIds: ids,
        photoEntities: entities,
      };
    }

    case actionTypes.toggleFavourite: {
      const currentIndex = state.favourites.findIndex(
        id => id === action.payload,
      );
      const favourites = [...state.favourites];

      if (currentIndex === -1) {
        favourites.push(action.payload);
      } else {
        favourites.splice(currentIndex, 1);
      }

      return {
        ...state,
        favourites,
      };
    }

    default:
      return state;
  }
}
