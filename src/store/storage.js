const storePrefix = 'gallery-app:';

function loadStorage(key = 'state') {
  return JSON.parse(localStorage.getItem(`${storePrefix}${key}`));
}

function setStorage(data, key = 'state') {
  localStorage.setItem(`${storePrefix}${key}`, JSON.stringify(data));
}

export function restoreState(initialState) {
  const storedState = loadStorage('state') || {};

  return {
    ...initialState,
    ...storedState,
  };
}

export const storeState = props => state => {
  const storedData = props.reduce(
    (acc, key) => ({ ...acc, [key]: state[key] }),
    {},
  );
  setStorage(storedData);
};
