const accessKey = process.env.REACT_APP_UNSPLASH_ACCESS_KEY;
const unsplashApiUrl = 'https://api.unsplash.com/';

function handleResponseStatus(response) {
  if (response.status !== 200) {
    throw new Error('Something went wrong with the response status.');
  }
}

function formatApiUrl(path, params, baseApi = unsplashApiUrl) {
  const url = new URL(`${baseApi}${path}`);
  if (params) {
    url.search = new URLSearchParams(params);
  }
  return url;
}

async function fetchUnsplashUrl(
  path,
  params,
  requestHeaders,
  authorization = `Client-ID ${accessKey}`,
) {
  const targetUrl = formatApiUrl(path, params);
  const headers = {
    ...requestHeaders,
    Authorization: authorization,
  };

  const response = await fetch(targetUrl, { headers });

  handleResponseStatus(response);
  const totalItems = +response.headers.get('X-Total');
  const limit = +response.headers.get('X-Ratelimit-Remaining');

  const data = await response.json();
  return { data, totalItems, limit };
}

async function fetchPhotoPage(page, perPage) {
  const result = await fetchUnsplashUrl('photos', { page, per_page: perPage });
  return {
    ...result,
    hasMore: page * perPage < result.totalItems,
  };
}

export default fetchPhotoPage;
