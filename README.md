# Unsplash gallery

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Prerequisites
The use the app you need a unsplash api access key.
It's read from an environment variable of `REACT_APP_UNSPLASH_ACCESS_KEY`

Either add it to a local `.env` file or set the appropriate variable.

For all other further steps you'll need to install dependencies by running `npm ci`.

## Overview
### Implementation notes
- Used [Unsplash API](https://unsplash.com/developers)
- Used hooks for the implementation of a primitive redux like state management.
- Implemented infinite scroll and lazy loading using [intersection observer](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API)
- Went with a simple single scss file for styling.
- Skipped on the tests.

### Possible improvements
- Add unit tests. Most likely would require mocking intersection observer.
- Use a better styling solution. Preferably something like styled components or css modules.
- Ideally use the above solution to consolidate lazy loaded image variables into one place.
- Virtual scrolling.
- Add placeholder image when loading.

### Possible feature suggestions
- Filter/page for viewing favourited items
- Ability to open photos in fullscreen/fullres view
- Extend fullscreen view to gallery mode
- Display remaining request limit
- Allow supplying of own token for requests

## Building
### `npm run build`
Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Development
### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
